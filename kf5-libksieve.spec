%global framework libksieve

Name:           kf5-%{framework}
Version:        23.08.5
Release:        3
Summary:        Sieve support library

License:        GPLv2
URL:            https://invent.kde.org/pim/%{framework}

%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0:        http://download.kde.org/%{stable}/release-service/%{version}/src/%{framework}-%{version}.tar.xz

# handled by qt5-srpm-macros, which defines %%qt5_qtwebengine_arches
%{?qt5_qtwebengine_arches:ExclusiveArch: %{qt5_qtwebengine_arches}}

BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qttools-static
BuildRequires:  qt5-qtwebengine-devel
BuildRequires:  ktextaddons-devel

%global kf5_ver 5.23.0

BuildRequires:  extra-cmake-modules >= %{kf5_ver}
BuildRequires:  kf5-rpm-macros >= %{kf5_ver}
BuildRequires:  kf5-karchive-devel >= %{kf5_ver}
BuildRequires:  kf5-kconfig-devel >= %{kf5_ver}
BuildRequires:  kf5-ki18n-devel >= %{kf5_ver}
BuildRequires:  kf5-kiconthemes-devel >= %{kf5_ver}
BuildRequires:  kf5-knewstuff-devel >= %{kf5_ver}
BuildRequires:  kf5-ktextwidgets-devel >= %{kf5_ver}
BuildRequires:  kf5-kwidgetsaddons-devel >= %{kf5_ver}
BuildRequires:  kf5-kwindowsystem-devel >= %{kf5_ver}
BuildRequires:  kf5-syntax-highlighting-devel >= %{kf5_ver}

#global majmin_ver %(echo %{version} | cut -d. -f1,2)
%global majmin_ver %{version}

BuildRequires:  kf5-akonadi-contacts-devel >= %{majmin_ver}
BuildRequires:  kf5-akonadi-server-devel >= %{majmin_ver}
BuildRequires:  kf5-kcalendarcore-devel
BuildRequires:  kf5-kidentitymanagement-devel >= %{majmin_ver}
BuildRequires:  kf5-kmailtransport-devel >= %{majmin_ver}
BuildRequires:  kf5-kmime-devel >= %{majmin_ver}
BuildRequires:  kf5-kpimtextedit-devel >= %{majmin_ver}
BuildRequires:  kf5-libkdepim-devel >= %{majmin_ver}
BuildRequires:  kf5-pimcommon-devel >= %{majmin_ver}

Obsoletes:      kdepim-libs < 7:16.04.0
Conflicts:      kdepim-libs < 7:16.04.0

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       cmake(KF5SyntaxHighlighting)
%description    devel
%{summary}.

%prep
%autosetup -n %{framework}-%{version}

%build
%{cmake_kf5}
%cmake_build

%install
%cmake_install

%find_lang %{name} --all-name --with-html

%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*
%{_kf5_datadir}/knsrcfiles/ksieve_script.knsrc
%{_kf5_datadir}/qlogging-categories5/*%{framework}.*
%{_kf5_datadir}/sieve/
%{_kf5_libdir}/libKPim5KManageSieve.so.*
%{_kf5_libdir}/libKPim5KSieve.so.*
%{_kf5_libdir}/libKPim5KSieveUi.so.*

%files devel
%{_kf5_archdatadir}/mkspecs/modules/qt_KManageSieve.pri
%{_kf5_archdatadir}/mkspecs/modules/qt_KSieveUi.pri
%{_includedir}/KPim5/KManageSieve/
%{_includedir}/KPim5/KSieveUi/
%{_kf5_includedir}/KSieve/libksieve_version.h
%{_kf5_libdir}/cmake/KPim5LibKSieve/
%{_kf5_libdir}/libKPim5KManageSieve.so
%{_kf5_libdir}/libKPim5KSieve.so
%{_kf5_libdir}/libKPim5KSieveUi.so


%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-3
- adapt to the new CMake macros to fix build failure

* Wed Mar 27 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-2
- fix build error of eulermake

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Mon Jan 08 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.4-1
- 23.08.4

* Fri Aug 04 2023 peijiankang <peijiankang@kylinos.cn> - 23.04.3-1
- 23.04.3

* Mon Jun 12 2023 misaka00251 <liuxin@iscas.ac.cn> - 22.12.0-1
- Init package
